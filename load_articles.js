$(document).ready(function() {
    $.ajax({
        url: "https://sporadic.nz/2018a_web_assignment_service/Articles",
        type: 'GET',
        success: function (msg) {
            console.log(msg);
            for(var i = 0; i < msg.length; i++){
                $(".navbar-nav ").append(
                    "<li class=\"nav-item\">" +
                    "<a class=\"nav-link\" href=\"#a"+msg[i].id+"\">Article" +msg[i].id +"</a>"+
                    "</li>"
                );

                $(".articleContainer").append(
                    "<div class=\"col-md-6\">" +
                    "<div class=\"panel panel-default article\">" +
                    "<div id=\"a" +msg[i].id+ "\" class=\"panel-heading\">" + msg[i].title + "</div>" +
                    "<div class=\"panel-body\">" +
                    "<img src=\"" + msg[i].imageUrl + "\">" +
                    "<p>" +msg[i].content+ "</p>" +
                    "</div>" +
                    "</div>");
            }
        }
    });
});

